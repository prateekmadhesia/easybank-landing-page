let headButton = document.getElementById("headButton");

headButton.addEventListener("click", afterClick);

function afterClick() {
  let classList = document.getElementById("headButton").classList;
  let topImage = document.getElementById("topImage").classList;
  let backImage = document.getElementById("backImage").classList;

  if (!classList.contains("head-button-mobile-line")) {
    classList.add("head-button-mobile-line");
    classList.remove("head-button-mobile-cross");

    document.getElementById("mobileNav").style.display = "none";
    document.getElementById('intro-imgs').style.height="100vh";
    
  } else {
    classList.remove("head-button-mobile-line");
    classList.add("head-button-mobile-cross");
    
    document.getElementById("mobileNav").style.display = "flex";
    document.getElementById('intro-imgs').style.height="400px";
  }

  if(topImage.contains("top-image-click")){
    topImage.remove("top-image-click");
  }else{
    topImage.add("top-image-click");
  }

  if(backImage.contains("back-image-click")){
    backImage.remove("back-image-click");
  }else{
    backImage.add("back-image-click");
  }
}
